#! /usr/bin/env python3
import os
from sys import argv
import logging
from datetime import datetime
from pathlib import Path


def update_log_file(title):
    log = Path.home() / ".jl"
    if not log.exists():
        log.touch()
    with open(log, "w+") as f:
        f.write(str(title))


def get_last_entry():
    log = Path.home() / ".jl"
    if not log.exists():
        raise ValueError("Log doesn't exist")
    last_entry = Path(log.read_text())
    if not last_entry.exists():
        raise ValueError("Log points to non-existent file")
    return last_entry


def title_taken_message(title):
    return "Sorry! The title \"" + title + "\" is already taken. :c"


def launch_editor(journal_file):
    editor_command = 'vim + "' + str(journal_file) + '"'
    logging.info("launching command: " + editor_command)
    os.system(editor_command)


def get_unique_title(journal_dir):
    title = input("Please provide a title or just say \"no\": ")
    if title == "no":
        return "untitled"
    file = journal_dir / (title + ".txt")
    if file.exists() or file == "":
        print(title_taken_message(title))
        return get_unique_title(journal_dir)
    else:
        return title

level = None
for arg in argv:
    if arg[0:6] == "--log=":
        level = arg[6:]
        levelNum = getattr(logging, level.upper())
        if not isinstance(levelNum, int):
            raise ValueError('Invalid log level: %s' % level)
        logging.basicConfig(format='%(levelname)s:%(message)s', level=levelNum)
        logging.info("custom logging level given: " + level)
        argv.remove(arg)
    if arg == "-p":
        launch_editor(get_last_entry())
        exit(0)

logging.info("jl starting...")

logging.info("args given:"+str(argv))

d = datetime.now().isoformat(sep='_', timespec='seconds')

if len(argv) < 2:
    title = "untitled"
else:
    title = argv[1]
logging.info("title = " + title)

# TODO change log to config
# TODO add support for setting journal directory in the config
if level and level.upper() == "INFO":
    journalDir = Path("/tmp")
else:
    journalDir = Path.home() / "Documents/journal"
logging.info("journal directory: " + str(journalDir))

if not journalDir.is_dir():
    journalDir.mkdir()
    logging.info(str(journalDir) + " directory was created")
else:
    logging.info(str(journalDir) + " directory was found")

if title != "untitled":
    # can't have forward slashes in the filename
    journalFile = journalDir / (title.replace("/","-") + ".txt")
else:
    journalFile = journalDir / ("untitled_" + d)

if journalFile.is_file():
    print(title_taken_message(title))
    title = get_unique_title(journalDir)
    if title != "untitled":
        journalFile = journalDir / (title + ".txt")
    else:
        journalFile = journalDir / ("untitled_" + d)


logging.info("creating new journal file: " + str(journalFile))
journalFile.touch()
with open(journalFile, "w+") as f:
    f.write(title + "\n\ncreated " + d + "\n\n\n")

launch_editor(journalFile)

if title == "untitled":
    title = get_unique_title(journalDir)
    if title != "untitled":
        newJournalFile = journalDir / (title + ".txt")
        journalFile.rename(newJournalFile)
        journalFile = newJournalFile
        with open(journalFile) as f:
            lines = f.readlines()
        lines[0] = title + "\n"
        with open(journalFile, "w") as f:
            f.writelines(lines)

update_log_file(journalFile)

# TODO add -r (recent) functionality for printing out last 10 journal entries in format "title.txt, date-stamp"
# TODO add -l (list) functionality for listing out all journal entries in format "title.txt, date-stamp"